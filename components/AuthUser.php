<?php

namespace app\components;

use app\models\User;
use Yii;


class AuthUser
{
    /**
     * @return bool
     */
    public static function isAuth()
    {
        return !Yii::$app->user->isGuest;
    }

    /**
     * @return User
     */
    public static function getUser()
    {
        if (!self::isAuth()) {
            return null;
        }
        return Yii::$app->user->identity;
    }

    /**
     * @return int|string|null
     */
    public static function getUserId()
    {
        if (!self::isAuth()) {
            return null;
        }
        return Yii::$app->user->getId();
    }

    /**
     * @param string|array $permissionName
     * @param null|string|array $params
     * @return bool
     */
    public static function userCan($permissionName, $params = null)
    {

        if (is_array($permissionName)) {
            if ($permissionName[0] == 'and') {
                foreach ($permissionName[1] as $permission) {
                    if (is_array($params)) {
                        foreach ($params as $param) {
                            if (!Yii::$app->user->can($permission, $param)) {
                                return false;
                            }
                        }
                    } else {
                        if (!Yii::$app->user->can($permission, $params)) {
                            return false;
                        }
                    }
                }
                return true;
            }

            if ($permissionName[0] == 'or') {
                foreach ($permissionName[1] as $permission) {
                    if (is_array($params)) {
                        foreach ($params as $param) {
                            if (Yii::$app->user->can($permission, $param)) {
                                return true;
                            }
                        }
                    } else {
                        if (Yii::$app->user->can($permission, $params)) {
                            return true;
                        }
                    }
                }
                return false;
            }

            return false;
        }

        return Yii::$app->user->can($permissionName, $params);
    }

    public static function getRoleName()
    {
        $authManager = Yii::$app->authManager;
        return $authManager->getRolesByUser(AuthUser::getUser()->id);
    }
}