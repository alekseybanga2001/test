<?php

namespace app\assets;

use ayrozjlc\blockui\BlockUiAsset;
use kartik\dialog\DialogAsset;
use Yii;
use yii\bootstrap4\BootstrapAsset;
use yii\helpers\Url;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css?v=refresh0.0.5',
        'https://use.fontawesome.com/releases/v5.8.1/css/all.css',
    ];

    public $js = [
        'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
        'vendor/quicksearch/jquery.quicksearch.js',
        'https://code.highcharts.com/highcharts.js',
        'https://code.highcharts.com/modules/exporting.js',
        'https://code.highcharts.com/modules/export-data.js',
        'https://code.highcharts.com/modules/accessibility.js',
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BlockUiAsset::class,
        DialogAsset::class,
    ];
}
