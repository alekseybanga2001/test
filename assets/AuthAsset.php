<?php

namespace app\assets;

use app\components\AddVarJs;
use Yii;
use yii\bootstrap4\BootstrapAsset;
use yii\helpers\Url;
use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AuthAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://use.fontawesome.com/releases/v5.8.1/css/all.css',
        'https://cdn3.devexpress.com/jslib/18.1.6/css/dx.common.css',
        'https://cdn3.devexpress.com/jslib/18.1.6/css/dx.light.css',
    ];

    public $js = [
        'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
        'js/addel.jquery.min.js?v=refresh2.0.61',
        'https://cdn3.devexpress.com/jslib/18.1.6/js/dx.all.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        BootstrapAsset::class,
        '\ayrozjlc\blockui\BlockUiAsset',
    ];
}
