* * * * * execute composer install --ignore-platform-reqs
* * * * * Create and connect the database (PostgreSQL)
* * * * * execute yii migrate
* * * * * execute yii rbac/init (to get user admin)
