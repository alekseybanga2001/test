<?php

$params = require __DIR__ . '/params.php';
$maindb = require __DIR__ . '/maindb.php';

$config = [
    'id' => 'basic',
    'name' => getenv('APP_NAME'),
    'language' => 'en',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'UTC',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@kvdrp' => '@app/vendor/kartik-v/yii2-date-range/src',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
        ],
        'i18n' => [
            'translations' => [
                'kvdrp' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@kvdrp/messages',
                    'forceTranslation' => true
                ],
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/language',
                ],
            ],
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
            'timeout' => 7 * 86400,
            'sessionTable' => '{{%user_session}}',
            'writeCallback' => function ($session) {
                if(Yii::$app->user){
                    return [
                        'user_id' => Yii::$app->user->getId(),
                        'ip' => Yii::$app->request->remoteIP,
                        'useragent' => Yii::$app->request->userAgent
                    ];
                }
                return [
                    'ip' => Yii::$app->request->remoteIP,
                    'useragent' => Yii::$app->request->userAgent
                ];
            }
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $maindb,
        'metadata' => ['class' => 'Metadata'],
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => ['en'],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableLanguageDetection' => false,
            'enableDefaultLanguageUrlCode' => false,
           // 'enableLocaleUrls' => false,
            'rules' => [
                '/' => 'auth/login',
                '/login' => 'auth/login',
                '<controller>/multi-action/<do>' => '<controller>/multi-action',
                '<controller>/<action>' => '<controller>/<action>',

            ],
        ],
        'formatter' => [
            'dateFormat' => 'php:d-m-Y',
            'datetimeFormat' => 'php:H:i:s d-m-Y',
            'timeFormat' => 'php:H:i:s',
            'nullDisplay' => '-'
        ],
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV3' => getenv('RECAPTCHA_SITE_KEY') ? getenv('RECAPTCHA_SITE_KEY') : '6Lfx96gkAAAAAIGyFOn2_PDhqKgc99Ze6c03G6p2',
            'secretV3' => getenv('RECAPTCHA_SECRET') ? getenv('RECAPTCHA_SECRET') : '6Lfx96gkAAAAAGdDQRmQwN98KoxgfQCzlCBbup2O',
        ]
    ],
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
        'api' => [
            'basePath' => '@app/modules/api',
            'class' => 'app\modules\api\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => explode(',', getenv('DEBUG_IP')) ?? ['127.0.0.1'],
        'historySize' => 1000,
    ];
}

return $config;
