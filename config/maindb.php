<?php

$masterHost = getenv('DB_HOST_MASTER');
$slaveHost = getenv('DB_HOST_SLAVE');
$port = getenv('DB_PORT');
$name = getenv('DB_NAME');
$user = getenv('DB_USER');
$pass = getenv('DB_PASSWORD');

$attributes = [
    PDO::ATTR_TIMEOUT => 10,
    PDO::ATTR_PERSISTENT => true
];

if ($slaveHost) {
    return [
        'class' => 'yii\db\Connection',
        'enableSchemaCache' => true,
        'serverStatusCache' => false,
        'schemaCacheDuration' => 3600,
        'schemaMap' => [
            'pgsql' => [
                'class' => \yii\db\pgsql\Schema::class,
                'columnSchemaClass' => [
                    'class' => \yii\db\pgsql\ColumnSchema::class,
                    'deserializeArrayColumnToArrayExpression' => false,
                ]
            ]
        ],
        'masterConfig' => [
            'username' => $user,
            'password' => $pass,
            'attributes' => $attributes
        ],
        'masters' => [
            ['dsn' => "pgsql:host={$masterHost};port={$port};dbname={$name}"],
        ],
//        'slaveConfig' => [
//            'username' => $user,
//            'password' => $pass,
//            'attributes' => $attributes
//        ],
//        'slaves' => [
//            ['dsn' => "pgsql:host={$slaveHost};port={$port};dbname={$name}"],
//        ],
    ];

}

return [
    'class' => 'yii\db\Connection',
    'dsn' => "pgsql:host={$masterHost};port={$port};dbname={$name}",
    'enableSchemaCache' => true,
    'username' => $user,
    'password' => $pass,
    'charset' => 'utf8',
    'attributes' => $attributes,
    'schemaMap' => [
        'pgsql' => [
            'class' => \yii\db\pgsql\Schema::class,
            'columnSchemaClass' => [
                'class' => \yii\db\pgsql\ColumnSchema::class,
                'deserializeArrayColumnToArrayExpression' => false,
            ]
        ]
    ],
];

