<?php

use app\migrations\Migration;
use app\models\User;
use app\models\UserSession;

/**
 * Class m140209_132010_table_user_session
 */
class m140209_132010_table_user_session extends Migration
{
    public function up()
    {
        $this->createTable(UserSession::tableName(), [
            'id' => $this->string(255),
            'expire' => $this->integer(),
            'user_id' => $this->integer(),
            'ip' => $this->string(),
            'useragent' => $this->string(),
            'data' => 'BYTEA',
        ], $this->tableOptions);

        $this->createIndex('{{%ip-user_session}}', UserSession::tableName(), ['ip']);
        $this->createIndex('{{%expire-user_session}}', UserSession::tableName(), ['expire']);
        $this->createIndex('{{%user_id-user_session}}', UserSession::tableName(), ['user_id']);
        $this->createIndex('{{%id-user_session}}', UserSession::tableName(), ['id']);
        $this->addPrimaryKey('id_user_session',UserSession::tableName(),['id'] );
        $this->addForeignKey('FK_user_to_session', UserSession::tableName(), 'user_id', User::tableName(), 'id','CASCADE');
    }

    public function down()
    {
        $this->dropTable(UserSession::tableName());
    }
}
