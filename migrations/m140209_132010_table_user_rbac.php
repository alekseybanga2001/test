<?php

use app\migrations\Migration;
use app\models\User;
use app\models\UserSession;

/**
 * Class m140209_132010_table_user_rbac
 */
class m140209_132010_table_user_rbac extends Migration
{
    /**
     * @return bool|void|null
     */
    public function up()
    {
        $yiiDir = Yii::getAlias('@app/yii');
        $command = "php $yiiDir migrate --interactive=0 --migrationPath=@yii/rbac/migrations/";
        echo shell_exec($command);
    }

    /**
     * @return bool|void|null
     */
    public function down()
    {
        $dir = Yii::getAlias('@app/yii');
        $dir = "php $dir migrate/down 999 --interactive=0 --migrationPath=@yii/rbac/migrations/";

        shell_exec($dir);
    }
}
