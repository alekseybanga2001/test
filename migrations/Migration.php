<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\migrations;

use Yii;

/**
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Migration extends \yii\db\Migration
{
    public $tableOptions = '';

    /**
     * @return string
     */
    public function uuidPrimaryKey(){
        return 'UUID PRIMARY KEY DEFAULT uuid_generate_v4()';
    }

    /**
     * @return string
     */
    public function uuidNotNull(){
        return 'UUID NOT NULL';
    }

    /**
     * @return string
     */
    public function uuidDefaultNull(){
        return 'UUID DEFAULT NULL';
    }

    /**
     * @return string
     */
    public function uuid(){
        return 'UUID';
    }
}