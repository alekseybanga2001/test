<?php
use app\migrations\Migration;
use app\models\User;
use app\models\UserLog;

class m140209_132010_table_user_log extends Migration
{
    public function up()
    {
        $this->createTable(UserLog::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'ip' => $this->string(),
            'controller' => $this->string(),
            'action' => $this->string(),
            'method' => $this->string(),
            'message' => $this->text(),
            'browser_finger' => $this->text(),
            'created_at' => $this->dateTime(),
        ], $this->tableOptions);


        $this->addForeignKey(
            'FK-user_log-to-user',
            UserLog::tableName(),
            'user_id',
            User::tableName(),
            'id','CASCADE');

        $this->createIndex('ip-index',UserLog::tableName(),['ip']);
        $this->createIndex('action-index',UserLog::tableName(),['action']);
        $this->createIndex('controller-index',UserLog::tableName(),['controller']);
        $this->createIndex('method-index',UserLog::tableName(),['method']);
        $this->createIndex('user_id-logs-index',UserLog::tableName(),['user_id']);
    }

    public function down()
    {
        $this->dropTable(UserLog::tableName());
    }
}