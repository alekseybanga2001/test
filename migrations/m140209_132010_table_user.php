<?php

use app\migrations\Migration;
use app\models\User;

/**
 * Class m140209_132017_table_user
 */
class m140209_132010_table_user extends Migration
{
    public function up()
    {
        $this->createTable(User::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $this->tableOptions);

        $this->createIndex('{{%id-index-user}}', User::tableName(), ['id']);
        $this->createIndex('{{%username-index-user}}', User::tableName(), ['username']);
    }

    public function down()
    {
        $this->dropTable(User::tableName());
    }
}