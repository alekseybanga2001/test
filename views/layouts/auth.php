<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AuthAsset;

AuthAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="<?=getenv('ALT_LOGO_LOGIN')??'/favicon.png'?>" sizes="32x32">
    <style>
        body {
            height: 100%;
            background-color: #ebf2f2 !important;
            font-family: 'Roboto', sans-serif;
            font-weight: 300;
            color: #363636;
        }
        .login-container {
            box-shadow: 0 0 33px 0px #0000003b;
            background: white;
            border-radius: 5px;
            border: 1px solid #ceccccc7;
            margin-top: 50px;
        }
        .form-group{
            margin-bottom: 0!important;
        }
        .logo{
            margin-top: 50px;
        }
    </style>
</head>
<?php if(getenv('ALT_LOGIN_BACKGROUND_IMG')){ ?>
<body style="background-image: url('<?=getenv('ALT_LOGIN_BACKGROUND_IMG')?>'); background-position: center;background-size: cover; ">
<?php }else{ ?>
<body>
<?php } ?>

<?php $this->beginBody() ?>

<div class="wrap">

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
