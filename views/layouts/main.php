<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\components\AddVarJs;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\AuthUser;
use app\rbac\Rbac;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="icon"
              href="<?= getenv('ALT_FAVICON') ? getenv('ALT_FAVICON') : (getenv('ALT_LOGO') ? getenv('ALT_LOGO') : '/favicon.png') ?>"
              sizes="32x32">
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrap">

        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => '/',
            'id' => 'main-navbar',
            'brandImage' => '',
            'options' => [
                    'style'=>'max-height: 25px',
                'class' => 'navbar navbar-expand-lg fixed-left navbar-light bg-light',
            ]
        ]);

        $items[] = [
            'label' => 'Main',
            'url' => ['/site/index'],
        ];
        $items[] = [
            'label' => 'Page A',
            'url' => ['/site/page-a'],
        ];
        $items[] = [
            'label' => 'Page B',
            'url' => ['/site/page-b'],
        ];

        $items[] = [
            'label' => '<i class="fas fa-user red"></i> User Statistic',
            'url' => ['/user-log/index'],
            'visible' => AuthUser::userCan(Rbac::ROLE_ROOT)
        ];


        $username = AuthUser::getUser()->username;
        $items[] = "<li><a>{$username}</a></li>";

        $items[] = [
            'label' => '<i class="fas fa-sign-out-alt"></i> ',
            'url' => ['/auth/logout'],
        ];

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $items,
            'id' => 'main-navbar-ty',
            'encodeLabels' => false
        ]);
        NavBar::end();
        ?>


        <div class="container-fluid">
            <?= Breadcrumbs::widget([
                'homeLink' => [
                    'label' => '#',
                    'url' => ['/'],
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <?php $this->endBody() ?>

    <footer class="bg-light text-center text-lg-start fixed-bottom">
        <!-- Copyright -->
        <div class="text-center p-3 text-muted">
            © <?= date('Y'); ?> <?= getenv('APP_NAME') ?>
            <a class="text-dark" href="/">Alpha 0.1</a>
        </div>
        <!-- Copyright -->
    </footer>

    </body>

    </html>
<?php $this->endPage() ?>