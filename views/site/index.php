<?php


use app\components\AuthUser;
use yii\helpers\Url;


$this->title = 'Index';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row text-center">

    <div class="col-md-12">
        <h1>Hello <?= AuthUser::getUser()->username ?>!</h1>
    </div>
    <div class="col-md-6">
        <h1>
            <a href="<?= Url::toRoute(['site/page-a']) ?>">
                <button class="btn btn-primary btn-lg">
                    Page A
                </button>
            </a>
        </h1>
    </div>
    <div class="col-md-6">
        <h1>
            <a href="<?= Url::toRoute(['site/page-b']) ?>">
                <button class="btn btn-danger btn-lg">
                    Page B
                </button>
            </a>
        </h1>
    </div>


</div>
