<?php


use app\components\AuthUser;
use yii\helpers\Url;


$this->title = 'Page B';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row text-center">
    <div class="col-md-12">
        <h1>
            <a href="<?= Url::toRoute(['site/download-exe']) ?>">
                <button class="btn btn-success btn-lg">
                    Download
                </button>
            </a>
        </h1>
    </div>
</div>
