<?php


use app\components\AuthUser;
use yii\helpers\Url;


$this->title = 'Page A';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row text-center">
    <div class="col-md-12">
        <?php if (!Yii::$app->session->get('buy-cow')) { ?>
            <h1>
                <a href="<?= Url::toRoute(['site/buy-cow']) ?>">
                    <button class="btn btn-success btn-lg">
                        Buy a cow
                    </button>
                </a>
            </h1>
        <?php } ?>
    </div>
</div>
