<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\forms\LoginForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;

$this->title = 'Sign-In';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4 text-center logo">
        <img src="<?= getenv('ALT_LOGO') ? getenv('ALT_LOGO') : '/img/logo.svg' ?>" alt="" style="width: 100%;">
    </div>
    <div class="col-sm-6 col-md-4 col-md-offset-4 login-container">
        <h3 class="text-center login-title"><?= Html::encode($this->title) ?></h3>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-signin'],
        ]); ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'username')->textInput()->label('Login') ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'password')->passwordInput() ?>
            </div>

            <div class="col-md-12">
                <?= Html::submitButton('<i class="fas fa-sign-in-alt"></i> Log in', ['class' => 'btn btn-success btn-block  mt-3 mb-3', 'name' => 'login-button']) ?>
            </div>
            <div class="col-md-12">
                <a href="<?= Url::toRoute(['auth/sign-up']) ?>">
                    <?= Html::button('<i class="fas fa-key"></i> Sign Up', ['class' => 'btn btn-primary btn-block  mt-3 mb-3', 'name' => 'login-button']) ?>
                </a>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>













