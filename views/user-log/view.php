<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\AuthUser;

/* @var $this yii\web\View */
/* @var $model \app\models\UserLog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dialplan-view">

    <?= DetailView::widget([
        'model' => $model,
    ]) ?>


</div>
