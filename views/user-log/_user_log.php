<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;

/* @var $dataProvider app\controllers\UserLogController */
/* @var $searchModel app\controllers\UserLogController */

?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'label' => 'User',
            'attribute' => 'user_id',
            'format' => 'raw',
            'filter' => Html::activeDropDownList(
                $searchModel,
                'user_id',
                ArrayHelper::map(\app\models\User::find()->all(), 'id', 'username'),
                ['class' => 'form-control', 'prompt' => 'All users']
            ),
            'value' => function ($model) {
                $tooltip = " data-toggle='tooltip' data-placement='top' title='{$model->browser_finger}' ";
                return "<span $tooltip>" . ($model->user->username ?? '-') . "</span>";
            }
        ],

        [
            'attribute' => 'ip',
            'format' => 'raw',
            'value' => function ($model) {
                $tooltip = " data-toggle='tooltip' data-placement='top' title='{$model->browser_finger}' ";
                return "<span $tooltip>" . ($model->ip) . "</span>";
            }
        ],
        [
            'attribute' => 'controller',
            'filter' => Html::activeDropDownList(
                $searchModel,
                'controller',
                ArrayHelper::map(\app\models\UserLog::find()->select('DISTINCT(controller)')->asArray()->cache(100)->all(), 'controller', 'controller'),
                ['class' => 'form-control', 'prompt' => '---']
            ),
        ],
        [
            'attribute' => 'action',
            'filter' => Html::activeDropDownList(
                $searchModel,
                'action',
                ArrayHelper::map(\app\models\UserLog::find()->select('DISTINCT(action)')->asArray()->cache(100)->all(), 'action', 'action'),
                ['class' => 'form-control', 'prompt' => '---']
            ),
        ],
        [
            'attribute' => 'method',
            'filter' => Html::activeDropDownList(
                $searchModel,
                'method',
                ArrayHelper::map(\app\models\UserLog::find()->select('DISTINCT(method)')->asArray()->cache(100)->all(), 'method', 'method'),
                ['class' => 'form-control', 'prompt' => '---']
            ),
        ],
        [
            'attribute' => 'message',
            'format' => 'raw',
            'value' => function ($model) {
                return is_array(json_decode($model->message)) ? (json_encode(json_decode($model->message)[0] ?? '')) : $model->message;
            }
        ],
        [
            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'data_time',
                'convertFormat' => true,
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => date("Y-m-d H:i") . ' - ' . date("Y-m-d H:i"),
                    //'readonly' => true,
                ],
                'pluginOptions' => [
                    'showDropdowns' => true,
                    'autoApply' => true,
                    'buttonClasses' => 'hidden',
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'timePickerSeconds' => true,
                    'locale' => [
                        'format' => 'Y-m-d H:i:s',
                    ],
                    'ranges' => [
                        Yii::t('kvdrp', "Today") => ["moment().startOf('day')", "moment().endOf('day')"],
                        Yii::t('kvdrp', "Yesterday") => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        Yii::t('kvdrp', "Last {n} Days", ['n' => 7]) => ["moment().startOf('day').subtract(6, 'days')", "moment().endOf('day')"],
                        Yii::t('kvdrp', "Last {n} Days", ['n' => 30]) => ["moment().startOf('day').subtract(29, 'days')", "moment().endOf('day')"],
                        Yii::t('kvdrp', "This Month") => ["moment().startOf('month')", "moment().endOf('month')"],
                        Yii::t('kvdrp', "Last Month") => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],

                    ]
                ],
            ]),
            'attribute' => 'created_at',
            'format' => 'datetime',
        ],
        [
            'format' => 'raw',
            'visible' => \app\components\AuthUser::userCan('debug'),
            'value' => function ($model) {
                return Html::a('Детально', ['user-log/view', 'id' => $model->id]);
            }
        ]
    ],
]); ?>
<?php Pjax::end(); ?>