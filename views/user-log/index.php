<?php

use yii\bootstrap4\Tabs;
use yii\helpers\Html;

/* @var $dataProvider app\controllers\UserLogController */
/* @var $searchModel app\controllers\UserLogController */
/* @var $date app\controllers\UserLogController */
/* @var $pageACount app\controllers\UserLogController */
/* @var $pageBCount app\controllers\UserLogController */
/* @var $buyCowCount app\controllers\UserLogController */
/* @var $downloadCount app\controllers\UserLogController */


$this->title = 'UserLog';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div>
    <?php echo Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => "User Log",
                'content' => $this->render('_user_log', compact('dataProvider', 'searchModel')),
                'active' => true,
            ],
            [
                'label' => "Statistics",
                'content' => $this->render('_user_statistic', compact('date', 'pageACount',
                    'pageBCount', 'buyCowCount', 'downloadCount')),
            ],
        ],
    ]); ?>
</div>

