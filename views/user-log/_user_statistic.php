<?php

/* @var $date app\controllers\UserLogController */
/* @var $pageACount app\controllers\UserLogController */
/* @var $pageBCount app\controllers\UserLogController */
/* @var $buyCowCount app\controllers\UserLogController */
/* @var $downloadCount app\controllers\UserLogController */
?>

<div>
    <figure class="highcharts-figure">
        <div id="container"></div>
    </figure>

</div>
<?php $this->registerJs("
     Highcharts.chart('container', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'User activity statistics',
            align: 'left'
        },
        xAxis: [{
            categories: {$date},
            crosshair: true
        }],
        yAxis: [{ 
            labels: {
                format: '{value} ',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            title: {
                text: 'Page A',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }
        }, { 
            title: {
                text: 'Page B',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value} ',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 80,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || // theme
                'rgba(255,255,255,0.25)'
        },
        series: [{
            name: 'Page A',
            type: 'column',
            yAxis: 1,
            data: {$pageACount},
            tooltip: {
                valueSuffix: ' times'
            }

        }, 
         {
            name: 'Page B',
            type: 'column',
            data: {$pageBCount},
            tooltip: {
                valueSuffix: ' times'
            }
        },
        {
            name: 'Buy Cow',
            type: 'column',
            data: {$buyCowCount},
            tooltip: {
                valueSuffix: ' times'
            }
        }, 
         {
            name: 'Download',
            type: 'column',
            data: {$downloadCount},
            tooltip: {
                valueSuffix: ' times'
            }
        }
        ]
    });
") ?>