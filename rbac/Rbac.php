<?php

namespace app\rbac;


class Rbac
{
    const USER_STATISTIC = 'user_statistic';

    const DEFAULT_ROOT_PERMISSIONS = [
        self::USER_STATISTIC,
    ];

    const USER_RULE = [
        self::USER_STATISTIC,
    ];

    const ROLE_ROOT = 'root';
    const ROLE_USER = 'user';
}