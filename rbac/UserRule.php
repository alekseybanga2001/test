<?php

namespace app\rbac;

use app\components\AuthUser;
use yii\rbac\Item;
use yii\rbac\Rule;

class UserRule extends Rule
{
    public $name = 'isUserRule';

    /**
     * @param int|string $user_id
     * @param Item $item
     * @param $params
     * @return bool
     */
    public function execute($user_id, $item, $params): bool
    {
        if (!AuthUser::userCan(Rbac::ROLE_ROOT)) {
            return false;
        }

        return true;
    }



}