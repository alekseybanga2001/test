<?php

namespace app\controllers;

use app\models\UserLog;
use app\rbac\Rbac;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\components\AuthUser;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [ 'index','page-a','page-b','buy-cow','download-exe'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => [ 'error'],
                        'allow' => true,
                    ]
                ]
            ]
        ];
    }
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        UserLog::add($action);
        return parent::beforeAction($action);
    }

    /**
     * @return string|Response
     */
    public function actionIndex()
    {
        if (AuthUser::userCan(Rbac::ROLE_ROOT)) {
            return $this->redirect(['user-log/index']);
        }
      return $this->render('index');
    }
    /**
     * @return string
     */
    public function actionPageA()
    {
          return $this->render('page-a');
    }

    /**
     * @return string
     */
    public function actionPageB()
    {
      return $this->render('page-b');
    }
    /**
     * @return Response
     */
    public function actionBuyCow()
    {
        Yii::$app->session->set('buy-cow',true);
        Yii::$app->session->setFlash('success', 'thankYou' );
        return $this->redirect(['site/index']);
    }
    /**
     * @return Response
     */
    public function actionDownloadExe()
    {
        $path = glob(Yii::getAlias('@app/runtime/test.exe'));
        return Yii::$app->response->sendFile($path[0]);
    }

}
