<?php

namespace app\controllers;

use app\models\search\UserLogSearch;
use app\models\UserLog;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use app\components\AuthUser;

/**
 * Class AutogotoifCustomController
 * @package app\controllers
 */
class UserLogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view','delete'],
                        'allow' => true,
                        'roles' => ['root'],
                    ]
                ]
            ],
        ];
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $searchModel = new UserLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $statistics = $searchModel->searchStatistic();

        $date = [];
        $pageACount = [];
        $pageBCount = [];
        $buyCowCount = [];
        $downloadCount = [];
        if($statistics){
            foreach ($statistics as $statistic){
                $date[]=explode(' ',$statistic['date'])[0];
                $pageACount[]=$statistic['count_page_a'];
                $pageBCount[]=$statistic['count_page_b'];
                $buyCowCount[]=$statistic['count_buy_cow'];
                $downloadCount[]=$statistic['count_download_exe'];
            }
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'date'=>json_encode($date),
            'pageACount'=>json_encode($pageACount),
            'pageBCount'=>json_encode($pageBCount),
            'buyCowCount'=>json_encode($buyCowCount),
            'downloadCount'=>json_encode($downloadCount),
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     * @return UserLog|array|\yii\db\ActiveRecord|null
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = UserLog::find()->where(['id' => $id])->one()) !== null) {

            if (AuthUser::userCan('root')) {
                return $model;
            }
            

            throw new ForbiddenHttpException('Permission denied');
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
