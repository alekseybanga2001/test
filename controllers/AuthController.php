<?php

namespace app\controllers;


use app\components\AuthUser;
use app\models\forms\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\forms\LoginForm;

/**
 *
 */
class AuthController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'sign-up'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $layout = AuthUser::isAuth()?'@app/views/layouts/main.php':'@app/views/layouts/auth.php';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => $layout
            ]
        ];
    }
    /**
     * @return string|Response
     */
    public function actionSignUp()
    {

        $this->layout = '@app/views/layouts/auth.php';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->redirect(['site/index']);
        }

        $model->password = '';
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = '@app/views/layouts/auth.php';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['site/index']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['auth/login']);
    }
}
