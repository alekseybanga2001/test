<?php /** @noinspection ALL */

namespace app\models;

use app\components\pbx\FastAccessPbx;
use app\models\interfaces\FormDataInterface;
use app\models\interfaces\UserStatusInterface;
use Yii;
use yii\base\Exception;
use app\models\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\IdentityInterface;
use app\components\AuthUser;
use app\rbac\Rbac;
use yii\helpers\ArrayHelper;

/**
 *
 * @property $id
 * @property $name
 * @property $username
 * @property $auth_key
 * @property $password_hash
 * @property $pbx_user_id
 * @property $status
 * @property $created_at
 * @property $updated_at
 *
 * @property Group[] $group
 * @property GroupUser[] $groupUser
 * @property Contact[] $contact
 * @property ContactReliable[] $contactReliableHistory
 * @property ContactComment[] $contactComment
 * @property ContactStatusHistory[] $contactStatusHistory
 * @property ContactDeal[] $contactDeal
 */
class User extends ActiveRecord implements IdentityInterface
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [TimestampBehavior::className()];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasMany(UserSession::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * @param $username
     * @return User|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if ($this->password_hash) {
            return Yii::$app->security->validatePassword($password, $this->password_hash);
        }
        return false;
    }

    /**
     * @param $password
     *
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString(255);
    }
}
