<?php

namespace app\models;

/**
 * Class UserSession
 *
 * @package app\models
 *
 * @property $id
 * @property $user_id
 * @property $ip
 * @property $useragent
 * @property string $expire [integer]
 * @property string $data [bytea]
 */
class UserSession extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_session}}';
    }
}
