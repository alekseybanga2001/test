<?php

namespace app\models\search;

use app\models\UserLog;
use kartik\daterange\DateRangeBehavior;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

/**
 * Class LogSearch
 * @package app\models\search
 */
class UserLogSearch extends UserLog
{
    public $data_time;
    public $createTimeStart;
    public $createTimeEnd;

    public function rules()
    {
        return [
            ['user_id', 'integer'],
            [['ip', 'controller', 'action', 'method', 'message'], 'string'],
            [['data_time'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'data_time',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     * @throws ForbiddenHttpException
     */
    public function search($params)
    {
        $query = self::find();
        $query->andWhere(['!=', 'user_id', 0]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['user_id' => $this->user_id]);
        $query->andFilterWhere(['controller' => $this->controller]);
        $query->andFilterWhere(['action' => $this->action]);
        $query->andFilterWhere(['method' => $this->method]);

        $query->andFilterWhere(['like', 'ip', $this->ip]);
        $query->andFilterWhere(['like', 'message', $this->message]);

        if ($this->data_time) {
            $query->andFilterWhere(['between', 'created_at', date("Y-m-d H:i:s", $this->createTimeStart),
                date("Y-m-d H:i:s", $this->createTimeEnd)]);
        }

        return $dataProvider;
    }

    public function searchStatistic()
    {
        $sub_query = self::find()
            ->select([
                'action',
                "DATE_TRUNC('day',created_at) as date",
            ]);

        $query = self::find()
            ->select([
                "count(*) FILTER (WHERE(action = 'page-a')) as count_page_a",
                "count(*) FILTER (WHERE(action = 'page-b')) as count_page_b",
                "count(*) FILTER (WHERE(action = 'buy-cow')) as count_buy_cow",
                "count(*) FILTER (WHERE(action = 'download-exe')) as count_download_exe",
                "date",
            ])
            ->from($sub_query)
            ->groupBy('date')
            ->orderBy('date')
            ->asArray();
        $collection = $query->all();
        return $collection;
    }
}
