<?php

namespace app\models;


use app\models\behaviors\TimestampBehavior;

/**
 * Class UserLog
 * @package app\models
 *
 * @property $id
 * @property $user_id
 * @property $ip
 * @property $controller
 * @property $action
 * @property $method
 * @property $message
 * @property $browser_finger
 * @property $created_at
 */
class UserLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_log}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']
                ]
            ]
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function add($action, $message = null)
    {
        $app = \Yii::$app;
        if (!$app->request->getIsPjax()) {
            $controller = $action->controller->id;
            $user_id = $app->user->getId();
            $new = new self();
            $new->user_id = $user_id;
            $new->ip = $app->request->remoteIP;
            $new->browser_finger = $app->request->getUserAgent();
            $new->controller = $controller;
            $new->action = $action->id;
            $new->method = $app->request->getMethod();
            $new->message = $message ?? json_encode([$app->request->get(), $app->request->post()]) ?? null;

            if ($last_record = self::find()->orderBy('id DESC')->one()) {
                if ($last_record->action != $action->id | $last_record->user_id != $user_id) {
                    $new->save();
                }
            } else {
                $new->save();
            }
        }
    }
}
