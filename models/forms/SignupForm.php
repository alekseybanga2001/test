<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;
use yii\db\Exception;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {

        $rules = [
            ['username', 'required'],
            ['username', 'string', 'max' => 255],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Login taken'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6]

        ];

        return $rules;
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        try {
            $user = new User();
            $user->username = $user->name = $this->username;
            $user->setPassword($this->password);
            $user->auth_key = Yii::$app->security->generateRandomString(30);
            $user->save();

        } catch (Exception $exception) {
            var_dump($exception->getMessage());
            exit();
            return null;
        }

        Yii::$app->user->login($user, 3600 * 24 * 30);
        return $user;
    }

}