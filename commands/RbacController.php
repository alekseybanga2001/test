<?php

namespace app\commands;

use app\models\User;
use app\rbac\ContactCallRule;
use app\rbac\ContactCommentRule;
use app\rbac\ContactDealRule;
use app\rbac\ContactReliableRule;
use app\rbac\ContactRule;
use app\rbac\ContactStatusHistoryRule;
use app\rbac\GroupRule;
use app\rbac\UserRule;
use Yii;
use yii\console\Controller;
use yii\rbac\ManagerInterface;
use yii\rbac\Permission;
use app\rbac\Rbac;

/**
 * @property ManagerInterface $authManager
 * @property Permission[] $permissions
 */
class RbacController extends Controller
{
    private $authManager;
    private $permissions;

    /**
     * @return void
     * @throws \yii\base\Exception
     */
    public function actionInit()
    {
        $this->authManager = \Yii::$app->authManager;

        if ($this->authManager->getRole(Rbac::ROLE_ROOT)) {
            exit('Already apply');
        }

        $this->authManager->removeAll();

        $rule = new UserRule();
        $this->authManager->add($rule);
        foreach (Rbac::USER_RULE as $permission_name) {
            $this->permissions[$permission_name] = $this->authManager->createPermission($permission_name);
            $this->permissions[$permission_name]->ruleName = $rule->name;
            $this->authManager->add($this->permissions[$permission_name]);
        }

        /**
         * ADD ROLES
         */
        $this->createRoleWithPermission(Rbac::ROLE_ROOT, Rbac::DEFAULT_ROOT_PERMISSIONS);
        $this->createDefaultUser();
    }

    /**
     * @param string $role
     * @param array $permissions
     * @return void
     * @throws \yii\base\Exception
     */
    private function createRoleWithPermission(string $role, array $permissions)
    {
        $role = $this->authManager->createRole($role);
        $this->authManager->add($role);
        foreach ($permissions as $permission) {
            $this->authManager->addChild($role, $this->permissions[$permission]);
        }
    }

    /**
     * @return void
     * @throws \yii\base\Exception
     */
    private function createDefaultUser(){
        $_password = Yii::$app->security->generateRandomString(15);
        $user = new User();
        $user->username = $user->name = 'admin_'.rand(1000,9999);
        $user->setPassword($_password);
        $user->auth_key = Yii::$app->security->generateRandomString(30);

        if ($user->save(false)) {
            $role = $this->authManager->getRole(Rbac::ROLE_ROOT);
            $this->authManager->assign($role, $user->id);
        }

        echo "\n\n";
        echo "Generated default admin user:\n";
        echo "Username: {$user->username}\n";
        echo "Password: {$_password}\n";
        echo "\n\n";
    }

}